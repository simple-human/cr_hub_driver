//! # cr_hub_driver
//!
//! A data source module for [control\_room](https://gitlab.com/simple-human/control_room) library.
//! It alows fetching data from other instances of Control-room servers. This makes possible building distributed data fetching systems.
//!
//!```
//! use control_room::{DriverRegister, SourcesConfig, Observables};
//! use cr_hub_driver::HubDriver;
//! use serde_json::json;
//! 
//! let drivers: DriverRegister = Default::default();
//! drivers.register::<HubDriver>("hub_source".into());
//! 
//! let sources_config: SourcesConfig = serde_json::from_value(json!(
//!     {
//!         "hub": {
//!                  "driver": "hub_source",
//!                  "options": {
//!                      "server": "wss://example.com:9001/control_room",
//!                      "user": "test_user",
//!                      "password": "test_password",
//!                  }
//!              },
//! 
//!     }
//! ))
//! .unwrap();
//! 
//! let observables: Observables = serde_json::from_value(json!(
//!         {
//!             "hub": {
//!                     "datasets": [{
//!                         "source": "hub",
//!                         "data_options":  {
//!                             "observable": "remote_observable",
//!                             "dataset_id": 0
//!                         },
//!                         "view_options": {
//!                             "name": "hub"
//!                         }
//!                     }],
//! 
//!                 "client": {
//!                     "title": "hub",
//!                     "description": "hub test",
//!                 }
//!             }
//!         }
//! ))
//! .unwrap();
//!
//!``` 
//!
//! Options for the source config are
//! - *server* - websocket address of remote server;
//! - *user* - login name at remote server;
//! - *password* - password for login.
//!
//! *data_options* for the dataset:
//! - *observable* - name of remote observable;
//! - *dataset_id* - id of dataset in corresponding remote observable's *datasets* array.



use futures::stream::BoxStream;
use futures::stream::StreamExt;
use futures::SinkExt;
use futures::{Sink, Stream};
use serde_derive::{Deserialize, Serialize};
use serde_json::{json, Value};
use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::mpsc::{channel, Sender};
use tokio::sync::Mutex;
use tokio::task::JoinHandle;
use tokio::time;

use control_room::{Configurator, Driver, Error};

use log::{debug, warn};
use websocket_lite::Message;

#[derive(Debug, Deserialize, Clone)]
struct HubDriverOptions {
    server: String,
    user: String,
    password: String,
    #[serde(default = "default_bufsize")]
    buf_size: usize,
    #[serde(default = "default_connection_timeout")]
    connection_timeout: u64,
}

fn default_bufsize() -> usize {
    100
}

fn default_connection_timeout() -> u64 {
    5000
}

pub struct HubDriver {
    hub: Arc<Mutex<Sender<DriverMsg>>>,
    buf_size: usize,
}

#[derive(Debug, Deserialize, Clone)]
struct ObservableOptions {
    observable: String,
    dataset_id: usize,
}

struct DriverMsg {
    controls: Arc<Value>,
    options: ObservableOptions,
    sender: Sender<Value>,
}

#[derive(Deserialize, Serialize)]
enum Data {
    #[serde(rename = "data")]
    Data {
        // TODO: change in server
        receiver: u64,
        dataset: usize,
        load: Value,
    },
}

impl Driver for HubDriver {
    fn observable(
        &self,
        options: Arc<Value>,
        controls: Arc<Value>,
    ) -> Result<BoxStream<'static, Result<Value, Error>>, Error> {
        let options: ObservableOptions = serde_json::from_value((*options).clone())
            .map_err(|e| Error::DriverError(format!("{}", e)))?;

        let (sender, mut receiver) = channel::<Value>(self.buf_size);

        let msg = DriverMsg {
            controls,
            options,
            sender,
        };
        let hub = self.hub.clone();

        let stream = async_stream::stream! {
            {
                let mut hub = hub.lock().await;
                hub.send(msg).await.map_err(|e| Error::DriverError(format!("{}", e)))?;
            }
            loop {
                if let Some(load) = receiver.recv().await {
                    yield Ok(load);
                }
            }

        };

        Ok(stream.boxed())
    }
}

impl Configurator for HubDriver {
    fn configure(options: Value) -> Result<Box<dyn Driver>, Error> {
        let options: HubDriverOptions =
            serde_json::from_value(options).map_err(|e| Error::DriverError(format!("{}", e)))?;

        let (driver_sender, mut driver_receiver) = channel::<DriverMsg>(options.buf_size);
        let buf_size = options.buf_size;

        let _: JoinHandle<Result<(), Error>> = tokio::spawn(async move {
            let mut client_max_id = 0u64;
            let clients: Arc<Mutex<HashMap<u64, DriverMsg>>> = Arc::new(Mutex::new(HashMap::new()));

            'main: loop {
                let connection = websocket_lite::ClientBuilder::new(&options.server)
                    .map_err(|e| Error::DriverError(format!("HubDriver: {}", e)))?;

                match connection.async_connect().await {
                    Ok(stream) => {
                        debug!("HubDriver connected to websocket");
                        // let stream = Mutex::new(stream);
                        let (ws_writer, mut ws_reader) = stream.split();
                        let ws_writer = Arc::new(Mutex::new(ws_writer));

                        {
                            let mut ws_writer = ws_writer.lock().await;
                            if ws_writer
                                .send(Message::text(
                                    json!(
                                    {"Auth":
                                        {
                                            "user": options.user,
                                            "password": options.password
                                        }})
                                    .to_string(),
                                ))
                                .await
                                .map_err(|e| warn!("HubDriver error: {} ", e))
                                .is_err()
                            {
                                warn!("HubDriver: authorization failed");
                                continue;
                            }

                            let msg: Option<websocket_lite::Result<Message>> =
                                ws_reader.next().await;
                            let msg = match msg {
                                Some(Ok(msg)) => Ok(msg),
                                _ => Err(Error::DriverError("bad websocket msg".into())),
                            }?;

                            let msg: Value = serde_json::from_slice(msg.data())
                                .map_err(|_| Error::DriverError("bad server response".into()))?;

                            if msg != json!({ "auth": true }) {
                                return Err(Error::DriverError("bad websocket msg".into()));
                            }

                            debug!("HubDriver authorized");

                            // get observables
                            let _: Option<websocket_lite::Result<Message>> = ws_reader.next().await;

                            // recover from disconect
                            let clients = clients.lock().await;

                            for (id, msg) in clients.iter() {
                                if let Err(_) = ws_writer
                                    .send(Message::text(driver_msg2subscribe_msg(id, &msg)))
                                    .await
                                {
                                    continue 'main;
                                }
                            }
                        }

                        let clients_subscribe = clients.clone();
                        let ws_writer_subscribe = ws_writer.clone();

                        let subscribe = async {
                            loop {
                                if let Some(msg) = driver_receiver.recv().await {
                                    let mut clients_lock = clients_subscribe.lock().await;
                                    let mut ws_writer = ws_writer_subscribe.lock().await;

                                    if let Ok(_) = ws_writer
                                        .send(Message::text(driver_msg2subscribe_msg(
                                            &client_max_id,
                                            &msg,
                                        )))
                                        .await
                                    {
                                        clients_lock.insert(client_max_id, msg);
                                        client_max_id += 1;
                                    } else {
                                        break;
                                    }
                                }
                            }
                        };

                        let read_data_or_unsubscribe = async {
                            loop {
                                if let Err(LocalError) =
                                    read_data_or_unsubscribe(&mut ws_reader, &ws_writer, &clients)
                                        .await
                                {
                                    break;
                                }
                            }
                        };

                        tokio::join!(subscribe, read_data_or_unsubscribe);
                    }
                    Err(e) => warn!("HubDriver error: {}", e),
                }

                warn!("HubDriver error: can't connect to server");
                let mut delay = time::interval(Duration::from_millis(options.connection_timeout));
                delay.tick().await;
                delay.tick().await;
            }
        });

        let driver = HubDriver {
            hub: Arc::new(Mutex::new(driver_sender)),
            buf_size,
        };

        Ok(Box::new(driver))
    }
}

fn driver_msg2subscribe_msg(id: &u64, msg: &DriverMsg) -> String {
    json!(
    {"Subscribe":
        {
            "subscriber": id,
            "observable": msg.options.observable,
            "controls": *msg.controls,
            "datasets": vec![msg.options.dataset_id],
        }})
    .to_string()
}

struct LocalError;
impl<T: std::error::Error> From<T> for LocalError {
    fn from(_: T) -> LocalError {
        LocalError
    }
}

async fn read_data_or_unsubscribe<R, W>(
    ws_reader: &mut R,
    ws_writer: &Arc<Mutex<W>>,
    clients: &Arc<Mutex<HashMap<u64, DriverMsg>>>,
) -> Result<(), LocalError>
where
    R: Stream<Item = Result<Message, Box<dyn std::error::Error + Sync + Send>>> + Unpin,
    W: Sink<Message, Error = Box<dyn std::error::Error + Sync + Send>> + Unpin,
{
    if let Some(Ok(msg)) = ws_reader.next().await {
        let Data::Data { load, receiver, .. } = serde_json::from_slice(msg.data())?;

        let mut clients = clients.lock().await;
        let mut ws_writer = ws_writer.lock().await;
        if let Some(driver) = clients.get_mut(&receiver) {
            if let Err(_) = driver.sender.send(load).await {
                clients.remove(&receiver);
                let unsubscribe = json!({ "Unsubscribe": receiver }).to_string();
                ws_writer
                    .send(Message::text(unsubscribe))
                    .await
                    .map_err(|_| LocalError)?;
            }
        }
        Ok(())
    } else {
        Err(LocalError)
    }
}
