# cr_hub_driver

A data source module for [control\_room](https://gitlab.com/simple-human/control_room) library.
It alows fetching data from other instances of Control-room servers. This makes possible building distributed data fetching systems.

```rust
use control_room::{DriverRegister, SourcesConfig, Observables};
use cr_hub_driver::HubDriver;
use serde_json::json;

let drivers: DriverRegister = Default::default();
drivers.register::<HubDriver>("hub_source".into());

let sources_config: SourcesConfig = serde_json::from_value(json!(
    {
        "hub": {
                 "driver": "hub_source",
                 "options": {
                     "server": "wss://example.com:9001/control_room",
                     "user": "test_user",
                     "password": "test_password",
                 }
             },

    }
))
.unwrap();

let observables: Observables = serde_json::from_value(json!(
        {
            "hub": {
                    "datasets": [{
                        "source": "hub",
                        "data_options":  {
                            "observable": "remote_observable",
                            "dataset_id": 0
                        },
                        "view_options": {
                            "name": "hub"
                        }
                    }],

                "client": {
                    "title": "hub",
                    "description": "hub test",
                }
            }
        }
))
.unwrap();

``` 

Options for the source config are
- *server* - websocket address of remote server;
- *user* - login name at remote server;
- *password* - password for login.

*data_options* for the dataset:
- *observable* - name of remote observable;
- *dataset_id* - id of dataset in corresponding remote observable's *datasets* array.

